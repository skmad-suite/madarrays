References
==========

    :Release: |release|
    :Date: |today|

madarrays\.masked\_array module
-------------------------------

.. automodule:: madarrays.mad_array
    :members:
    :undoc-members:
    :show-inheritance:

madarrays\.waveform module
--------------------------

.. automodule:: madarrays.waveform
    :members:
    :undoc-members:
    :show-inheritance:
    
