##############################
:mod:`madarrays` documentation
##############################

Overview
========
:mod:`madarrays` is a python package dedicated to the manipulation of audio
arrays with missing entries.

:mod:`madarrays` provides:

* a generic data structure `MadArray` to manipulate numpy arrays with missing
  entries
* a data structure `Waveform` to handle waveforms and easily get access to
  useful attributes and methods
* amplitude and phase masking for complex entries.

Documentation
=============

.. only:: html

    :Release: |version|
    :Date: |today|

.. toctree::
    :maxdepth: 1

    installation
    references
    tutorials
    credits


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`




