Tutorials
#########

Data structures
===============
These tutorials present data structures introduced in the module.

.. toctree::
    :maxdepth: 1
 
    _notebooks/mad_array.ipynb
    _notebooks/waveform.ipynb
