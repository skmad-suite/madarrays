MadArrays
=========

MadArrays is a Python package that regroups various data structures adapted to
handle audio signals with missing data.

Install
-------

Install the current release with ``pip``::

    pip install madarrays

For additional details, see doc/install.rst.

Usage
-----

See the `documentation <http://skmad-suite.pages.lis-lab.fr/madarrays/>`_.

Bugs
----

Please report any bugs that you find through the `MadArrays GitLab project
<https://gitlab.lis-lab.fr/skmad-suite/madarrays/issues>`_.

You can also fork the repository and create a merge request.

Source code
-----------

The source code of MadArrays is available via its `GitLab project
<https://gitlab.lis-lab.fr/skmad-suite/madarrays>`_.

You can clone the git repository of the project using the command::

    git clone git@gitlab.lis-lab.fr:skmad-suite/madarrays.git

Copyright © 2017-2018
---------------------

* `Laboratoire d'Informatique et Systèmes <http://www.lis-lab.fr/>`_
* `Université d'Aix-Marseille <http://www.univ-amu.fr/>`_
* `Centre National de la Recherche Scientifique <http://www.cnrs.fr/>`_
* `Université de Toulon <http://www.univ-tln.fr/>`_

Contributors
------------

* `Ronan Hamon <mailto:ronan.hamon@lis-lab.fr>`_
* `Valentin Emiya <mailto:valentin.emiya@lis-lab.fr>`_
* `Florent Jaillet <mailto:florent.jaillet@lis-lab.fr>`_

License
-------

Released under the GNU General Public License version 3 or later
(see `LICENSE.txt`).
